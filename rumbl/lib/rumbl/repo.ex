defmodule Rumbl.Repo do
  use Ecto.Repo, otp_app: :rumbl
end

#  @moduledoc """
#  In memory repository.
#  """

#  def all(Rumbl.User) do
#    [
#      %Rumbl.User{id: "1", name: "Michael Baggott", username: "abitdodgy", password: "abitdodgy"},
#      %Rumbl.User{id: "2", name: "Andrew Harrison", username: "sami", password: "sami"},
#      %Rumbl.User{id: "3", name: "Brian Modra", username: "dani", password: "dani"},
#    ]
#  end
#
#  def all(_module) do
#  end
#
#  def get(module, id) do
#	Enum.find all(module), fn map ->
#      map.id == id
#	end
#  end
#
#  def get_by(module, params) do
#    Enum.find all(module), fn map ->
#      Enum.all?(params, fn {key, value} ->
#        Map.get(map, key) == value 
#	  end)
#    end
#  end
#end